import pygame

class Entity:
    def __init__(self, x, y):
        self.x, self.y = x, y
        self.char = '.'
        self.color = pygame.Color(255, 255, 255)
        self.solid = True
        self.hitting = False
        self.remove = False
        self.opaque = False
        self.ticked = False

    def tick(self, lvl, actions):
        """Called once each game tick."""
        pass

    def hit(self, lvl, obj):
        """Called once when another entity occupies the same coordinates."""
        pass

class Wall(Entity):
    def __init__(self, x, y):
        super(Wall, self).__init__(x, y)
        self.color = pygame.Color(190, 190, 190)
        self.opaque = True
        self.char = '#'

class Floor(Entity):
    def __init__(self, x, y):
        super(Floor, self).__init__(x, y)
        self.color = pygame.Color(40, 40, 40)
        self.char = '.'
        self.solid = False

