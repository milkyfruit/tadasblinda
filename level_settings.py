import random
import utils

def get_level_settings(game_mode, depth):
    return gen_fun[game_mode](depth)

def get_level_settings_minimalist(depth):
    ret={}
    ret['min_dist']=5
    ret['room_biggening']=3 #TODO: play with this
    if depth<3:
        ret['no_intersect']=False
    else:
        ret['no_intersect']=True


    if depth<7:
        ret['terminate_early']=0.5 # lots of windy passages
    elif depth<15:
        ret['terminate_early']=0.5-(depth-7)*0.057
    else:
        ret['terminate_early']=0.1


    if depth<8:
        ret['num_room_tiles']=int(100-(depth/8)*50)
    else:
        ret['num_room_tiles']=0

    corridor_tile_counts=[200,150,200,275]
    if depth<4:
        ret['num_corridor']=corridor_tile_counts[depth]
    elif depth<10:
        ret['num_corridor']=300-(depth-4)*20
    else:
        ret['num_corridor']=180+(depth-10)*20 # need place for guards

    ret['counts']={}
    if depth<3:
        ret['counts']['number_guards']=2
    elif depth<5:
        ret['counts']['number_guards']=3
    else:
        ret['counts']['number_guards']=depth-1


    if depth>8:
        ret['counts']['number_kittens']=random.randint(0,1) #very advanced hidden kittens
    else:
        ret['counts']['number_kittens']=0

    if depth>2:
        ret['counts']['number_spiders']=random.randint(0,depth-1)
    else:
        ret['counts']['number_spiders']=0

    if depth>7:
        ret['counts']['number_smoke_bug']=random.randint(0,3)
    elif depth>3:
        ret['counts']['number_smoke_bug']=random.randint(0,1)
    else:
        ret['counts']['number_smoke_bug']=0

    if depth<1:
        ret['counts']['number_items']=0
    elif depth<5:
        ret['counts']['number_items']=1
    else:
        ret['counts']['number_items']=random.randint(1,2)
    return ret
    
def get_level_settings_default(depth):
    ret={'num_corridor': 200,
         'num_room_tiles': 100,
         'terminate_early': 0.1,
         'room_biggening': 3,
         'no_intersect': False,
         'min_dist': 0
         }
    return ret

def get_level_settings_level_editor(depth):
    pass

gen_fun = {utils.gamemodes[0]: get_level_settings_default,
           utils.gamemodes[1]: get_level_settings_default,
           utils.gamemodes[2]: get_level_settings_default,
           utils.gamemodes[3]: get_level_settings_minimalist,
           utils.gamemodes[5]: get_level_settings_level_editor
           }
