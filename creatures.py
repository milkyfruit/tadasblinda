import pygame
import utils, inventory
import entity, guard, player, traps, critters
import random, math

class Dragon(guard.Hostile):
    """A stationary dragon with thick scales and fiery breath. Drops an item if slain. Hoard not included."""
    def __init__(self, x, y, color=None):
        super().__init__(x, y)
        self.hitting = True #will slay any guards silly enough to venture close
        self.solid = False #you can sneak under it, if you can
        self.char = 'D'
        #however much I like the idea of black dragons, current grid visibility does not permit it
        self.color = color if color else pygame.Color(random.randint(100,255),0,0)
        self.viewdist = 4 #the dragon is sleepy if not disturbed
        self.health = 3
        self.view_angle = math.pi*2 #operating by smell
        self.breath_cd = 0
        self.player_pos_time = 10
        self.look_direction = random.random()*2*math.pi
        self.station = (self.x, self.y)
        self.attack=None
        
    def hit(self, room, obj):
        """Can take two hits, normal guards cannot harm him, and will stop a boulder (if it can)."""
        if isinstance(obj, guard.Guard) or isinstance(obj, player.Player) or isinstance(obj, Fire): return
        self.health-=1
        self.viewdist=7 #wake up
        if isinstance(obj, traps.Crossbow_Bolt) or isinstance(obj, traps.Boulder):
            self.look_direction = (obj.direction+2%4)*math.pi/2
            self._regen_viewcone(room, False, self.viewdist, self.view_angle)
            self.attack = utils.offsets4[(obj.direction+2)%4]
        self.view_angle = math.pi*1.1
        if self.health and isinstance(obj, traps.Boulder):
            self.heath-=1
            obj.remove=True
        if self.health<=0 and not self.remove:
            self.remove = True
            room.objects[(self.x,self.y)].append(inventory.Item(self.x, self.y))
            
    def tick(self, room, actions):
        """Breathe fire at any players coming too close."""
        #check if we see the player
        self.player_pos_time=max(10,self.player_pos_time+1)
        self.breath_cd=max(0, self.breath_cd-1)
        if self._see_player(room):
            self.viewdist=7 #wake up
            self.look_direction = math.atan2(self.nearest_player.y-self.y, self.nearest_player.x-self.x)
            self.view_angle = math.pi*1.1
            self._regen_viewcone(room, False, self.viewdist, self.view_angle)
            self.last_seen_player_pos = (self.nearest_player.x, self.nearest_player.y)
            self.player_pos_time = 0
            self.attack=None
        
        #if we do, or did recently, breathe fire at him/her, if we can
        if self.player_pos_time<=2:
            if self.breath_cd<=0:
                self.breath_cd = random.randint(4,6)
                dx, dy = self.last_seen_player_pos[0]-self.x, self.last_seen_player_pos[1]-self.y
                dd = ((dx>0)-(dx<0),(dy>0)-(dy<0))
                room.objects[(self.x+dd[0],self.y+dd[1])].append(Dragon_Fire(self.x+dd[0],self.y+dd[1], dd, (abs(dx), abs(dy)), (abs(dy),abs(dx)), 8))
            #if we cannot breathe, but can reach him, go there and maul him dead
            elif self.player_pos_time==0 and abs(self.last_seen_player_pos[1]-self.y)+abs(self.last_seen_player_pos[0]-self.x)==1:
                self.x, self.y = self.last_seen_player_pos
        elif self.attack:
            if self.breath_cd==0 and random.random()<0.8:
                self.breath_cd = random.randint(4,6)
                room.objects[(self.x+self.attack[0],self.y+self.attack[1])].append(Dragon_Fire(self.x+self.attack[0],self.y+self.attack[1], self.attack, (abs(self.attack[0]), abs(self.attack[1])), (abs(self.attack[1]),abs(self.attack[0])), 8))
            self.attack=None
        else:
            #turn bit by bit
            self.look_direction=(self.look_direction+random.gauss(0,0.3))%(math.pi*2)
            self._regen_viewcone(room, False, self.viewdist, self.view_angle)
            if (self.x,self.y)!=self.station:
                self.x,self.y=self.station
        self._draw_viewcone(room, pygame.Color(100,100,0))
        
class Fire(entity.Entity):
    def __init__(self, x, y, off_chance=0.5):
        super().__init__(x,y)
        self.hitting=True
        self.solid=True
        self.opaque=True
        self.ticked=True
        self.char='F'
        self.color=pygame.Color(255,100,50)
        self.off_chance=off_chance
        
    def hit(self, room, obj):
        if isinstance(obj,traps.Boulder): self.remove=True
        
    def tick(self, room, actions):
        for obj in room.objects[(self.x, self.y)]:
            if isinstance(obj,critters.Smoke): self.remove=True
        if not self.remove and random.random()<self.off_chance:
            self.remove=True
            if random.random()<0.2:
                room.objects[(self.x, self.y)].append(critters.Smoke(self.x, self.y))
        
class Dragon_Fire(Fire):
    def __init__(self, x, y, dir, cum, prop, TTL):
        super().__init__(x, y)
        self.direction = dir
        self.TTL = TTL
        self.propagation = prop
        self.cumulative = cum
        
    def tick(self, room, actions):
        self.remove=True
        #chance to spawn a remnant fire
        room.objects[(self.x, self.y)].append(Fire(self.x, self.y))
        #check if it ran out of momentum
        if self.TTL<=0 or (self.TTL==1 and random.random()<0.5): return
        #spread if not
        else:
            #spread straight forward and a bit to the side
            if self.cumulative[0]>self.cumulative[1]:
                for r in (((self.x+self.direction[0],self.y-1),(self.x+self.direction[0],self.y+1),(self.x+self.direction[0],self.y))):
                    if room.is_walkable(r[0],r[1]) and (random.random()<0.5 or abs(r[1]-self.y-self.direction[1])<2):
                        room.objects[(r[0],r[1])].append(Dragon_Fire(r[0],r[1], self.direction, (self.cumulative[0], self.cumulative[1]+self.propagation[0]), self.propagation, self.TTL-1))
            elif self.cumulative[0]<self.cumulative[1]:
                for r in ((self.x+1,self.y+self.direction[1]),(self.x-1,self.y+self.direction[1]),(self.x,self.y+self.direction[1])):
                    if room.is_walkable(r[0],r[1]) and (random.random()<0.5 or abs(r[0]-self.x-self.direction[0])<2):
                        room.objects[(r[0],r[1])].append(Dragon_Fire(r[0],r[1], self.direction, (self.cumulative[0]+self.propagation[1], self.cumulative[1]), self.propagation, self.TTL-1))
            else:
                if (room.is_walkable(self.x+self.direction[0],self.y) or room.is_walkable(self.x,self.y+self.direction[1])) and room.is_walkable(self.x+self.direction[0],self.y+self.direction[1]):
                    room.objects[(self.x+self.direction[0],self.y+self.direction[1])].append(Dragon_Fire(self.x+self.direction[0],self.y+self.direction[1], self.direction, (self.cumulative[0]+self.propagation[1], self.cumulative[1]+self.propagation[0]), self.propagation, self.TTL-1))
                for r in ((self.x+self.direction[0],self.y),(self.x,self.y+self.direction[1])):
                    if room.is_walkable(r[0],r[1]):
                        room.objects[(r[0],r[1])].append(Dragon_Fire(r[0],r[1], self.direction, (self.cumulative[0]+self.propagation[1], self.cumulative[1]+self.propagation[0]), self.propagation, self.TTL-1))
