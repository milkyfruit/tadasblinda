import random
import pygame
import utils, effects2
import traps, critters
import entity
import player
import guard

"""item list structure: name, description, activation message, color, (glyph), (additional message)"""
item_list = [
['Clear potion','A clear liquid inside transparent bottle.', 'You are almost invisible. For a while.', pygame.Color(200, 200, 200)], 
['Blue potion', 'Blue light blinks around in a bottle.', 'You teleported!', pygame.Color(75, 75, 255), '!', 'You did not teleport.'],
['Gray potion', 'A bottle with foggy liquid inside. You think you should not drink it.', 'You disappear in a cloud of smoke.', pygame.Color(100, 100, 100)], 
['Yellow potion', 'A bottle with tiny human inside.', 'Doppelganger created.', pygame.Color(200, 200, 0)],
['Placeable trap', 'You can place this trap and hope for a guard to step on it.', 'Trap set up. Do not get caught in it yourself.', pygame.Color(255, 0, 0)],
['Strange sapling', 'Is that blood on the roots?', 'Tree planted.', pygame.Color(55,255,55), 'y', 'Ground is too dry.'],
['Crossbow bolt', 'A harbinger of swift death.', 'Bolt shot.', pygame.Color(100,100,100), '|'],
['Purple potion', 'The liquid in this bottle swirles unceasingly.', 'The world slows down.', pygame.Color(200,0,200)]
]

random_table = [0, 1, 2, 3, 4, 7]

class Item(entity.Entity):
    def __init__(self, x, y, n=None):
        super(Item, self).__init__(x, y)
        if n is None: n = random.choice(random_table)
        self.id = n
        self.name = item_list[n][0]
        self.description = item_list[n][1]
        self.function = item_list[n][2]
        self.color = item_list[n][3]
        self.char = ('!' if (len(item_list[n])<=4) else item_list[n][4])
        self.solid = False

    def hit(self, lvl, obj):
    	if obj in lvl.players:
            if len(obj.inventory.contents) < 9 and not self.remove:
                self.remove = True
                obj.inventory.add(self)
            else:
                obj.inventory.set_message("Inventory full.")

class Sapling(Item):
    def __init__(self, x, y): super().__init__(x, y, 5)
class Bolt(Item):
    def __init__(self, x, y): super().__init__(x, y, 6)

def gen_random_item(lvl, i = -1):
	x, y = list(lvl.walkable)[random.randint(0, len(list(lvl.walkable))-1)]
	if i == -1:
        # choose random potion
        # if more items implemented add index to list
        # TODO implement item index 3
		i = random.choice(random_table)
	return gen_item(x, y, i, lvl)

def gen_item(x, y, item, lvl):
	if item in [i[0] for i in item_list]:
		item = [i[0] for i in item_list].index(item)
	elif not (isinstance(item, int) and 0 <= item < len(item_list)):
		return False
	lvl.objects[(x, y)].append(Item(x, y, item))
	return True
    
def generate(lvl, walkable, number_items=4):
    item_types = [{'n': random.choice(random_table)} for i in range(number_items)]
    utils.place(lvl, walkable, Item, number_items, 3, 1, pass_on=item_types)

def add_exit(lvl, difficulty, is_sapling):
    if is_sapling:
        utils.place(lvl, lvl.walkable, Item, 1, 1, 1, pass_on=[{'n':5}])
    else:
        utils.place(lvl, lvl.walkable, critters.The_Tree, 1, 1, 1, pass_on=[{'progress_required':difficulty}])
        
def blink_object(lvl, obj, dist, direction=None):
    """Move an object in a specified direction firing the blink effect"""
    if direction is None:
        effects2.animator.message_prompt("Choose a direction?")
        direction = utils.get_direction_key()
    if direction is None: return False
    effects2.animator.animate_movement(obj, direction, dist, lvl, 25, pygame.Color(100,100,255), 40)
    dx, dy = utils.offsets4[direction]
    obj.x, obj.y = obj.x+dx*dist, obj.y+dy*dist
    if not lvl.is_walkable(obj.x, obj.y):
        obj.hit(lvl, effects2.Blink_Effect())
        if not (obj.x, obj.y) in lvl.objects.keys():
            lvl.objects[(obj.x, obj.y)] = [obj]
    return True


class Inventory:
    def __init__(self):
        self.contents = []
        self.message = ''
        self.message_display_duration = 0
        self.message_color = None

    def add(self, item):
        self.contents.append(item)
        self.set_message(item.description)
        return True

    def remove(self, item):
        c = self.contents
        if item in utils.item_action_letters and utils.item_action_letters.index(item) < len(c):
            item = c[utils.item_action_letters.index(item)]
        elif not item in c:
            return False
        c.remove(item)
        return item
        
    def get_by_letter(self, item):
        """Get item reference from inventory letter."""
        c = self.contents
        if item in utils.item_action_letters and utils.item_action_letters.index(item) < len(c):
            item = c[utils.item_action_letters.index(item)]
        elif not item in c:
            return None
        return item

    def set_message(self, message, duration = 3, color = None):
        self.message = message
        self.message_display_duration = duration
        self.message_color = color

    def use_item(self, obj, item_id, lvl):
        item = self.get_by_letter(item_id)
        if item:
            if self.__item_effect(obj, item, lvl):
                self.remove(item_id)
                self.set_message(item.function)
                return True #will only return true if item used successfully
            elif len(item_list[item.id])>=6:
                self.set_message(item_list[item.id][5])
        return False
                
    def __item_effect(self, obj, item, lvl, direction=None):
        if item.name == item_list[0][0]:
            obj.invisible += 8 # n ticks of invisibility
        elif item.name == item_list[1][0]:
            return blink_object(lvl, obj, 5)
        elif item.name == item_list[2][0]:
            cloud_radius = 3
            spiders = (random.randint(0,42)==13)
            if spiders: self.set_message('As you open the bottle spiders pour out. Wait, what?')
            x, y = (obj.x, obj.y)
            cloud = []
            for i in range(1-cloud_radius, cloud_radius):
                obj.ninja_smoke = True
                for j in range(abs(i)-cloud_radius+1, cloud_radius-abs(i)):
                    cloud.append((x+i, y+j))
            for c in cloud:
                if c in lvl.objects.keys():
                    is_wall = any([isinstance(i, entity.Wall)
                       for i in lvl.objects[c]
                       ])
                    if not is_wall:
                        if spiders and (c[0]!=x or c[1]!=y): lvl.objects[c].append(critters.Spiderling(c[0], c[1]))
                        elif not spiders: lvl.objects[c].append(critters.Smoke(c[0], c[1], pygame.Color(100, 100, 100)))
        elif item.name == item_list[3][0]:
            # doppleganger
            lvl.players.append(player.Player(obj.x, obj.y))
            lvl.objects[(obj.x, obj.y)].append(lvl.players[-1])
        elif item.name == item_list[4][0]:
            lvl.objects[(obj.x,obj.y)].append(traps.PlaceableTrap(obj.x, obj.y))
        elif item.name == 'Strange sapling':
            if lvl.get_floor(obj.x, obj.y).color==pygame.Color(40,40,40):
                self.set_message(item_list[5][5])
                return False
            else:
                lvl.objects[(obj.x, obj.y)].append(critters.The_Tree(obj.x, obj.y, lvl.exit_difficulty))
        elif item.name == 'Crossbow bolt':
            if direction is None:
                effects2.animator.message_prompt("Choose the direction?")
                direction = utils.get_direction_key()
            if direction is None: return False
            lvl.objects[(obj.x, obj.y)].append(traps.Crossbow_Bolt(obj.x, obj.y, direction))
        elif item.id == 7:
            obj.haste+=4
            obj.haste_turn=True
        return True

    def render(self, console, show, dx=2, dy=2):
        if show:
            if self.contents:
                for j in range(len(self.contents)):
                    text = utils.item_action_letters[j] + ') ' + self.contents[j].name
                    console.print(dx, j + dy, text)
            else:
                text = 'Empty'
                console.print(dx, dy, text)
        if self.message and self.message_display_duration:
            # centering message
            start = console.width // 2 - len(self.message) // 2
            console.print(start, 0, self.message, self.message_color)
            self.message_display_duration -= 1
        #Add a bar at the bottom showing current items.
        for i in range(len(self.contents)):
            console.print((i*2)+1, console.height-1, self.contents[i].char, self.contents[i].color)
