import pygame
from enum import Enum
import math, copy, random
from collections import deque

item_action_letters = 'qweasdzxc'
item_action_keys = [[pygame.K_q], [pygame.K_w], [pygame.K_e], [pygame.K_a], [pygame.K_s], [pygame.K_d], [pygame.K_z], [pygame.K_x], [pygame.K_c]]
item_actions = list(zip(item_action_letters, item_action_keys))
# directions order: right, down, left, up
direction_keys = [(0, [pygame.K_RIGHT]), (1, [pygame.K_DOWN]), (2, [pygame.K_LEFT]), (3, [pygame.K_UP])]
no_action = [(999, [pygame.K_SPACE])]
other_actions = [('l', [pygame.K_l])]
actions = direction_keys + item_actions + no_action + other_actions
gamemodes = ("Tutorial", "Campaign", "Survival", "Classic campaign", "Custom levels", "Level editor")

def list2d(width, height, elem):
    """Returns 2d list of elem repeated width x height times."""
    return [[elem] * height for _ in range(width)]

offsets4 = [(1, 0), (0, 1), (-1, 0), (0, -1)] 
offsets8 = [
    (1, 0), (1, 1),
    (0, 1), (-1, 1),
    (-1, 0), (-1, -1),
    (0, -1), (1, -1)
] 

def dfs(start_x, start_y, is_walkable_fn, visit_fn, connectivity=offsets4):
    """Depth First Search on a 2d grid. Vists all walkable positions
    reachable from start, calls visit_fn for each one."""

    # Use stack instead of recursive dfs in order not to risk exploding callstack.
    visited = {}
    stack = [(start_x, start_y)]

    while len(stack) > 0:
        x, y = stack[-1]
        stack = stack[:-1]
        visited[(x, y)] = True
        visit_fn(x, y)

        for dx, dy in connectivity:
            nx, ny = x+dx, y+dy
            if not (nx, ny) in visited and is_walkable_fn(nx, ny):
                stack.append((nx, ny))                

def bfs(start_x, start_y, is_walkable_fn, end_x, end_y, connectivity=offsets4):
    """Breadth First Search on a 2d grid. Visits all walkable positions
    reachable from start."""
    #define initial variables
    connectivity=copy.deepcopy(connectivity)
    random.shuffle(connectivity) #randomize the search order to make the guards less boring
    connectivity=tuple(connectivity)
    start_pos=(start_x, start_y)
    end_pos=(end_x, end_y)
    visited = set()
    paths = dict()
    check = deque((start_pos,))
    #go over check while adding to visited and logging path to paths
    while True:
        try: cur_pos=check.pop()
        except IndexError: return [] #went to all possible locations and found no path
        for move in connectivity:
            target=(cur_pos[0]+move[0],cur_pos[1]+move[1])
            if target in visited: continue
            visited.add(target)
            if is_walkable_fn(target[0], target[1]):
                paths[target]=cur_pos
                check.appendleft(target)
                if target==end_pos:
                    path=[end_pos]
                    while path[-1]!=start_pos: path.append(paths[path[-1]])
                    path.reverse()
                    return path

        
def raycast(sx, sy, ex, ey, fn_check, fn_save):
    """Go in a straight line from (sx, sy), to (ex, ey), calling fn_save in each square. fn_check indicates whether a square is passable.
    The implementation was hit with a hammer until it stopped going through diagonal walls, so it uses integer arithmetic exclusively."""
    dx=1 if ex>sx else -1
    dy=1 if ey>sy else -1
    difx, dify = abs(sx-ex), abs(ey-sy)
    cumx, cumy = dify*2+difx, difx*2+dify
    difx*=2
    dify*=2
    while sx!=ex or sy!=ey:
        if fn_check(sx,sy):
            fn_save(sx,sy)
            if cumx>cumy:
                cumy+=difx
                sy+=dy
            elif cumx<cumy:
                cumx+=dify
                sx+=dx
            else:
                if fn_check(sx+dx,sy) or fn_check(sx,sy+dy):
                    sx+=dx
                    sy+=dy
                    cumx+=dify
                    cumy+=difx
                else: return
        else: return
    if fn_check(ex, ey): fn_save(ex, ey) #reached only if the whole path is passable
        
    
def viewcone(start_x, start_y, is_walkable_fn, radius, direction, view_angle=math.pi/2):
    ret={}
    def visitor(x,y):
        ret[(x,y)]=True
    if type(direction)!=float:
        direction=math.atan2(direction[1], direction[0])
    step=0.1
    angle=-view_angle/2
    while angle<=view_angle/2:
        ex=start_x+math.cos(angle+direction)*radius
        ey=start_y+math.sin(angle+direction)*radius
        #print(ex,ey)
        raycast(start_x,start_y,round(ex),round(ey),is_walkable_fn,visitor)
        angle=angle+step
        #print(angle)
    return ret

def get_direction_key():
    """Function to catch the next keypress and return its direction. Returns None if any other key is pressed."""
    while True:
        for event in pygame.event.get():
            if (event.type == pygame.locals.KEYDOWN) or (event.type == pygame.QUIT):
                for a in direction_keys:
                    for key in a[1]:
                        if event.key == key:
                            return a[0]
                return None

def place(lvl, walkable, obj, num, min_between=1, min_player=1, rooms_only=False, overlap=False, pass_on=None):
    """Generate objects on the walkable grid with minimum distance constraints."""
    assert (pass_on is None) or (len(pass_on)==num) or (isinstance(pass_on,dict) and num==1)
    if isinstance(pass_on,dict): pass_on=[pass_on]
    
    #As a warning, currently not all maps HAVE rooms, so if you're using rooms_only=True, sometimes nothing will be generated
    if rooms_only: walkable=generate_room_walkable(lvl, walkable)
    walkable=copy.deepcopy(walkable)
    if min_player:
        for p in lvl.players:
            px, py = p.x, p.y
            for i in range(px-min_player+1,px+min_player):
                for j in range(py-min_player+1,py+min_player):
                    walkable.pop((i,j),None)
    for l in range(num):
        if len(walkable)<=0:
            print("Not all objects generated.")
            return l
        coord=random.choice(list(walkable.keys()))
        if not overlap:
            while len(lvl.objects[coord])>1:
                walkable.pop(coord)
                if len(walkable)<=0:
                    print("Not all objects generated.")
                    return l
                coord=random.choice(list(walkable.keys()))
        if pass_on is None: lvl.objects[coord].append(obj(coord[0],coord[1]))
        else: lvl.objects[coord].append(obj(coord[0],coord[1],**pass_on[l]))
        if min_between:
            cx, cy = coord
            for i in range(cx-min_between+1,cx+min_between):
                for j in range(cy-min_between+1,cy+min_between):
                    walkable.pop((i,j),None)
    return num

def generate_room_walkable(lvl, walkable):
    """Return a list of walkable squares which can be went around, i.e. are parts of rooms (or ends of coridors)."""
    try: return lvl.walkable_rooms
    except AttributeError:
        lvl.walkable_rooms = copy.deepcopy(walkable)
        for x,y in walkable.keys():
            valid_dirs=[]
            for dx,dy in offsets4:
                if (x+dx,y+dy) in walkable: valid_dirs.append((dx,dy))
            if len(valid_dirs)<=1: lvl.walkable_rooms.pop((x,y))
            elif len(valid_dirs)>=2:
                diags=set()
                for i in range(len(valid_dirs)-1):
                    for j in range(i+1,len(valid_dirs)):
                        diags.add(tuple(map(sum, zip(valid_dirs[i], valid_dirs[j]))))
                        diags.discard((0,0))
                        diags_walk = [((k[0]+x,k[1]+y) in walkable) for k in diags]
                if sum(diags_walk)==0 or (len(valid_dirs)>2 and sum(diags_walk)<3): lvl.walkable_rooms.pop((x,y))
        return lvl.walkable_rooms
