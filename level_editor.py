import pygame
from pygame.locals import *
import level, inventory
import utils
import entity

# list of level editor objects
editor_item_list = [
['Floor', '.', pygame.Color(40, 40, 40)],
['Player', '@', pygame.Color(255, 255, 255)],
['Exit', '>', pygame.Color(0, 255, 0)],
['Guard', 'G', pygame.Color(120,30,40)],
['Crossbow Guard', 'B', pygame.Color(120,30,40)],
['Pot', 'p', pygame.Color(180,90,45)],
['Boulder trap', '+', pygame.Color(123,123,123)],
['Power source', 'Q', pygame.Color(55,155,55)],
['Spider', 's', pygame.Color(50,220,100)],
['Snake', '$', pygame.Color(55,155,55)],
['Cat', 'c', pygame.Color(120,120,120)],
['Random item', '?', pygame.Color(255,255,255)]
]


for i in inventory.item_list:
    if len(i) >= 5: editor_item_list.append([i[0], i[4], i[3]])
    else: editor_item_list.append([i[0], '!', i[3]])

class Level_Editor():
    def __init__(self, width, height, console, screen):
        self.clockmaster = pygame.time.Clock()
        self.console = console
        self.lvl = Level(width, height)
        self.screen = screen
        
    def main(self):
        need_rewall = False
        need_redraw = True
        help = False
        while True:
            actions = []
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return None
                elif event.type == KEYDOWN:
                    mods = pygame.key.get_mods()
                    if (event.key == K_F4 and (mods & KMOD_ALT)) or (event.key == K_q and (mods & KMOD_CTRL)):
                        return None
                    if event.key == K_ESCAPE:
                        return False
                    elif event.key == K_F1:
                        help = not help
                        need_redraw = True
                    elif event.key == K_F8:
                        if self.lvl.objects:
                            if self.lvl.create_level_file('level_editor_test.txt'):
                                self.lvl.inventory.set_message('Level saved to file ' + 'level_editor_test.txt', 99999)
                            else:
                                self.lvl.inventory.set_message('Failed to save file.', 99999)
                            need_redraw = True
                    elif not (event.key == K_BACKSPACE):
                        if event.key == K_TAB:
                            self.lvl.inventory.toggle()
                            need_redraw = True
                        else:
                            for a in utils.item_actions:
                                for key in a[1]:
                                    if event.key == key:
                                        actions.append(a)
                elif event.type == MOUSEBUTTONDOWN:
                    placement_object = self.lvl.inventory.placement_object
                    mouse_mode = 'add'
                    x, y = pygame.mouse.get_pos()
                    x = x // self.console.font_width
                    y = y // self.console.font_height - 1
                    if 0 < x < self.console.width-2 and 0 < y < self.console.height-2:
                        if placement_object[0] == 'Floor':
                            if (x, y) in self.lvl.walkable:
                                del self.lvl.walkable[(x, y)]
                                del self.lvl.objects[(x, y)]
                                mouse_mode = 'remove'
                            else:
                                self.lvl.walkable[(x, y)] = True
                            need_rewall = True
                        else:
                            if (x, y) in self.lvl.walkable:
                                if any([i.char == placement_object[1] for i in self.lvl.objects[(x, y)]]):
                                    for o in self.lvl.objects[(x, y)]:
                                        if o.char == placement_object[1]:
                                            to_be_removed = o
                                            break
                                    self.lvl.objects[(x, y)].remove(to_be_removed)
                                    mouse_mode = 'remove'
                                else:
                                    self.lvl.objects[(x, y)].append(entity.Entity(x, y))
                                    self.lvl.objects[(x, y)][-1].char = placement_object[1]
                                    self.lvl.objects[(x, y)][-1].color = placement_object[2]
                        need_redraw = True
                elif event.type == MOUSEMOTION and pygame.mouse.get_pressed()[0]:
                    placement_object = self.lvl.inventory.placement_object
                    x, y = pygame.mouse.get_pos()
                    x = x // self.console.font_width
                    y = y // self.console.font_height - 1
                    if 0 < x < self.console.width-2 and 0 < y < self.console.height-2:
                        if self.lvl.inventory.placement_object[0] == 'Floor':
                            if mouse_mode == 'add':
                                if not (x, y) in self.lvl.walkable:
                                    self.lvl.walkable[(x, y)] = True
                            elif mouse_mode == 'remove':
                                if (x, y) in self.lvl.walkable:
                                    del self.lvl.walkable[(x, y)]
                            need_rewall = True
                        else:
                            if (x, y) in self.lvl.walkable:
                                if mouse_mode == 'add':
                                    if not any([i.char == placement_object[1] for i in self.lvl.objects[(x, y)]]):
                                        self.lvl.objects[(x, y)].append(entity.Entity(x, y))
                                        self.lvl.objects[(x, y)][-1].char = placement_object[1]
                                        self.lvl.objects[(x, y)][-1].color = placement_object[2]
                                elif mouse_mode == 'remove':
                                    if any([i.char == placement_object[1] for i in self.lvl.objects[(x, y)]]):
                                        for o in self.lvl.objects[(x, y)]:
                                            if o.char == placement_object[1]:
                                                to_be_removed = o
                                                break
                                        self.lvl.objects[(x, y)].remove(to_be_removed)
                        need_redraw = True
            if self.lvl is not None and self.lvl.tick_editor(actions):
                need_redraw = True
            if need_rewall:
                self.lvl.put_walls()
                need_rewall = False
            if need_redraw:
                self.console.clear()
                self.lvl.render(self.console)
                self.lvl.tint.render(self.console)
                self.lvl.inventory.render(self.console)
                self.screen.fill(pygame.Color(0,0,0))
                if help: menu.show_help(self.console, self.screen, True)
                self.console.render(self.screen)
                need_redraw = False
                
            pygame.display.flip()
            self.clockmaster.tick(60)
            
class Level(level.Level):
    def __init__(self, width, height):
        super().__init__(width, height, "Level editor", 0)
        self.inventory = Inventory('Editor mode')

    # For level editor.
    def put_walls(self):
        """Puts floors and walls around walkable tiles."""

        def visit(x, y):
            if (x, y) in self.objects:
                self.objects[(x, y)].insert(0, entity.Floor(x, y))
            else:
                self.objects[(x, y)] = [entity.Floor(x, y)]
            for dx, dy in utils.offsets8:
                nx, ny = x+dx, y+dy
                if not (nx, ny) in self.walkable:
                    self.objects[(nx, ny)] = [entity.Wall(nx, ny)]
        self.objects = {k:v for k, v in self.objects.items() if not isinstance(v[0], entity.Wall)}
        for key, floor in self.walkable.items():
            if floor:
               x, y = key
               visit(x, y)
               
    def tick_editor(self, actions):
        for action in actions:
            if action[0] in utils.item_action_letters:
               return self.inventory.use_item(self, action[0], self)
        return False

    def create_level_file(self, filename, tier=0):
        def get_corner_coords(objects):
            first = True
            for x, y in objects:
                if first:
                    tx, ty = (x, y) # top left corner coordinates
                    bx, by = (x, y) # bottom right corner coordinates
                    first = False
                else:
                    if tx > x: tx = x
                    elif bx < x: bx = x
                    if ty > y: ty = y
                    elif by < y: by = y
            return tx, ty, bx, by

        tx, ty, bx, by = get_corner_coords(self.objects)
        output = [[' '] * (bx-tx+1) for _ in range(by-ty+1)]
        for k, v in self.objects.items():
            x, y = k
            output[y-ty][x-tx] = v[-1].char
        with open('levels\\'+filename, 'w') as f:
            f.write(str(tier) + '\n')
            for l in output:
                f.write(''.join(l) + '\n')
        return True
        
class Inventory(inventory.Inventory):
    def __init__(self, editor_mode=True):
        super().__init__()
        self.editor_mode = editor_mode
        self.show = False
        if self.editor_mode:
            self.contents = editor_item_list
            self.menu_position = 0
            self.placement_object = editor_item_list[0]
            self.set_message(str(self.placement_object[0] + ' selected for placement.'), 999999)
            

    def get_by_letter_editor(self, item):
        """Get item reference from inventory letter. Altered for use in level editor mode."""
        c = self.contents
        if item in utils.item_action_letters and utils.item_action_letters.index(item) < len(c):
            item = utils.item_action_letters.index(item)
            if len(c) <= 9:
                item = c[utils.item_action_letters.index(item)]
            else:
                if item != 0 and item != 8:
                    item = c[(item+self.menu_position-1) % len(c)]
                else:
                    if item == 0:
                        self.menu_position = (self.menu_position - 7) % len(c)
                    elif item == 8:
                        self.menu_position = (self.menu_position + 7) % len(c)
                    return 'Changed'
        elif not item in c:
            return None
        return item
        
    def toggle(self):
        self.show = not self.show
        
    def use_item(self, obj, item_id, lvl):
        item = self.get_by_letter_editor(item_id)
        if item == 'Changed':
            return True
        elif item:
            self.placement_object = item
            self.set_message(str(item[0] + ' selected for placement.'), 999999)
            return True
        return False
        
    def add(self, item):
        if not self.editor_mode:
            self.contents.append(item)
            self.set_message(item.description)
        return True
        
    def render(self, console, dx=2, dy=2):
        if self.show:
            if self.contents:
                if self.editor_mode:
                    if len(self.contents) <= 9:
                        for j in range(len(self.contents)+2):
                            text = utils.item_action_letters[j] + ') ' + contents[j][0] + ' ' + contents[j][1]
                            console.print(dx, j + dy, text)
                    else:
                        k = self.menu_position
                        c = self.contents
                        if k+7 >= len(c):
                            displayed_items = self.contents[k:] + self.contents[:k+7 % len(c)]
                        else:
                            displayed_items = self.contents[k:k+7]
                        contents = [['prev page', '']] + \
                                   displayed_items + \
                                   [['next page', '']]
                        for j in range(9):
                            text = utils.item_action_letters[j] + ') ' + contents[j][0] + ' ' + contents[j][1]
                            console.print(dx, j + dy, text)
            else:
                text = 'Empty'
                console.print(dx, dy, text)
        if self.message and self.message_display_duration:
            # centering message
            start = console.width // 2 - len(self.message) // 2
            console.print(start, 0, self.message, self.message_color)
            self.message_display_duration -= 1