import pygame
import utils, entity, critters
import random

class Animator():
    """Helper class to facilitate non-turn-based animation and contain references to the rendering framework."""
    def __init__(self, console, screen, w, h):
        """Initiate the helper class and pass it the relevant references."""
        self.console=console
        self.screen=screen
        self.clockmaster=pygame.time.Clock()
        self.x_scale=self.console.font_width
        self.y_scale=self.console.font_height
        self.width=w
        self.height=h
        self.need_redraw=False

    def lightning(self, x, y, lvl, direction=None, color=pygame.Color(200,220,255)):
        """Create a charging effect and then a lightning effect at the specified coordinates."""
        #get input for direction and make multiplication variables for drawing
        if direction is None:
            self.message_prompt("Choose the direction?")
            direction=utils.get_direction_key()
        if direction is None: return False
        a,b=utils.offsets4[direction]
        a*=self.x_scale
        b*=self.y_scale
        #clear the screen
        self.console.render(self.screen)
        #save the screen without any effects for adding each frame
        blur_surface=pygame.Surface((self.width,self.height))
        blur_surface.fill(pygame.Color(0,0,0))
        self.console.render(blur_surface)
        blur_surface.set_alpha(40) #fadeout rate
        #follow the lightning path and find the ending wall, while zapping spiders in the way
        pos_x, pos_y = x, y
        e = True
        while e:
            pos_x += utils.offsets4[direction][0]
            pos_y += utils.offsets4[direction][1]
            for obj in lvl.objects[(pos_x,pos_y)]:
                if isinstance(obj,entity.Wall):
                    length=max(abs(pos_x-x)+abs(pos_y-y),2)
                    e=False
                elif isinstance(obj, critters.Bug):
                    obj.hit(lvl, Lightning())
        #do the animation
        for f in range(60):
            self.clockmaster.tick(60) #framerate control
            self.screen.blit(blur_surface, (0,0)) #fade out the old lightning
            #draw the charging circle
            if f in range(30):
                pygame.draw.circle(self.screen,color,(round((x+0.5)*self.x_scale), round((y+1.5)*self.y_scale)),
                                   round(self.x_scale*((35-f)**1.7)//70), round(0.3*self.x_scale))
            #draw the lightning, one line at a time
            if f in range(15,60):
                points=[(round((x+0.5)*self.x_scale), round((y+1.5)*self.y_scale))]+[
                    (x*self.x_scale+a*i*2+round(random.gauss(self.x_scale/2,self.x_scale/3)),
                     (y+1)*self.y_scale+b*i*2+round(random.gauss(self.y_scale/2,self.y_scale/3))) for i in range(1,length//2+1)]
                pygame.draw.lines(self.screen, color, False, points)
            pygame.display.flip()
        #clear the screen
        self.screen.fill(pygame.Color(0,0,0))
        self.console.render(self.screen)
        pygame.display.flip()
        return True

    def animate_movement(self, obj, direction, distance, lvl, speed, bg=None, fadeout=100):
        """Animate the movement of a given object in a given direction at a given speed (1/speed sq/s)"""
        #find a replacement object and draw it over our object
        rep = (lvl.objects[(obj.x, obj.y)][-1] if lvl.objects[(obj.x, obj.y)][-1] is not obj else lvl.objects[(obj.x, obj.y)][-2])
        self.console.print(obj.x, obj.y+1, obj.char, obj.color)
        #clear the screen
        self.console.render(self.screen)
        #save the screen without any effects for adding each frame
        blur_surface=pygame.Surface((self.width,self.height))
        blur_surface.fill(pygame.Color(0,0,0))
        self.console.render(blur_surface)
        blur_surface.set_alpha(fadeout) #fadeout rate
        #animate the movement
        dx, dy = utils.offsets4[direction]
        for d in range(1,distance+1):
            self.screen.blit(blur_surface, (0,0)) #overwrite with the prerendered surface
            #and render our object further on
            x, y = obj.x+dx*d, obj.y+1+dy*d
            self.screen.blit(self.console.font.render(obj.char, True, obj.color, self.console.bg[x][y] if bg is None else bg),
                             pygame.Rect(x*self.x_scale, y*self.y_scale, (x+1)*self.x_scale, (y+1)*self.y_scale))
            pygame.display.flip()
            self.clockmaster.tick(speed) #enforce framerate
             
    def message_prompt(self, message, color=None):
        """Write an immediate message at the top without waiting for re-render. It will be overwritten with the next redraw."""
        if color is None: color = self.console.selected_fg
        self.console.render(self.screen)
        topbar=pygame.Surface((self.width,1))
        topbar.fill(self.console.selected_bg)
        start = self.console.width // 2 - len(message) // 2
        rect = pygame.Rect(start*self.x_scale, 0, (start+len(message))*self.x_scale, self.y_scale)
        text = self.console.font.render(message, True, color, self.console.selected_bg)
        self.screen.blit(text, rect)
        pygame.display.flip()
        self.need_redraw=True
        
    def selection_circle(self, x, y, radius=2, speed=60, color=pygame.Color(255,255,255)):
        """Create a shrinking circle effect at the specified coordinates."""
        #clear the screen
        self.console.render(self.screen)
        #save the screen without any effects for adding each frame
        blur_surface=pygame.Surface((self.width,self.height))
        blur_surface.fill(pygame.Color(0,0,0))
        self.console.render(blur_surface)
        blur_surface.set_alpha(60) #fadeout rate
        #do the animation
        for f in range(23):
            self.clockmaster.tick(speed) #framerate control
            self.screen.blit(blur_surface, (0,0)) #fade out the old effect
            #draw the charging circle
            pygame.draw.circle(self.screen,color,(round((x+0.5)*self.x_scale), round((y+1.5)*self.y_scale)),
                               round(self.x_scale*((35-f)**1.7)*radius//260), round(0.1*self.x_scale))
            pygame.display.flip()
        #clear the screen
        self.screen.fill(pygame.Color(0,0,0))
        self.console.render(self.screen)
        pygame.display.flip()
        return True
        
class Magic():
    """Semi-abstract class for inheritance."""
    def hit(self, lvl, obj): pass
    def tick(self, lvl, actions): pass
class Lightning(Magic):
    """Abstract class for damage sourcing."""
    pass
class Blink_Effect(Magic):
    """Abstract class for damage sourcing."""
    pass
