import random, copy
import utils
import entity


def maze1(width,height,ox=0,oy=0,sparsity=120,connectivity=20):
    """Makes a maze of twisty passages, all alike."""
    #randomly place around some rooms
    roompoints=set(zip(list(range(2+ox,width-2+ox))*(height-4),sorted(list(range(2+oy,height-2+oy))*(width-4))))
    available=set(zip(list(range(1+ox,width-1+ox))*(height-2),sorted(list(range(1+oy,height-1+oy))*(width-2))))
    walkable=set()
    connections=set()
    rooms=set()
    for i in range(width*height//sparsity):
        if not roompoints: break
        r=random.choice(tuple(roompoints))
        for x in range(-3,4):
            for y in range(-3,4):
                roompoints.discard((r[0]+x,r[1]+y))
        for x in zip((4,)*4+(-4,)*4+(-2,-1,1,2)*2,(-2,-1,1,2)*2+(4,)*4+(-4,)*4):
            roompoints.discard((r[0]+x[0],r[1]+x[1]))
        for x in range(-2,3):
            for y in range(-2,3):
                available.discard((r[0]+x,r[1]+y))
        for x in range(-1,2):
            for y in range(-1,2):
                walkable.add((r[0]+x,r[1]+y))
                rooms.add((r[0]+x,r[1]+y))
        if r[0]>=4:
            connections.add((r[0]-3,r[1]))
            walkable.add((r[0]-3,r[1]))
            walkable.add((r[0]-2,r[1]))
        if r[1]>=4:
            connections.add((r[0],r[1]-3))
            walkable.add((r[0],r[1]-3))
            walkable.add((r[0],r[1]-2))
        if r[0]<width-4:
            connections.add((r[0]+3,r[1]))
            walkable.add((r[0]+3,r[1]))
            walkable.add((r[0]+2,r[1]))
        if r[1]<height-4:
            connections.add((r[0],r[1]+3))
            walkable.add((r[0],r[1]+3))
            walkable.add((r[0],r[1]+2))
    #a bit of random walk between the rooms
    for i in connections:
        cd=None
        br=False
        cp=i
        lt=None
        while True:
            valid_dirs=[]
            for d in utils.offsets4:
                if (cd and d==(-cd[0],-cd[1])) or lt==d: continue
                if ts(cp,d) in available: valid_dirs.append(d)
                if ts(cp,d) in walkable and cd: br=True
            if br or not valid_dirs: break
            if cd and cd in valid_dirs: valid_dirs+=[cd]*2
            nd=random.choice(valid_dirs)
            if cd is not None and nd!=cd: lt=(-cd[0],-cd[1])
            else: lt=None
            cd=nd
            cp=ts(cp,cd)
            walkable.add(cp)
    #check for connectivity
    r1=random.choice(tuple(connections))
    for i in connections:
        if not utils.bfs(r1[0], r1[1], lambda x, y: (x,y) in walkable, i[0], i[1]):
            p=utils.bfs(r1[0], r1[1], lambda x, y: (x,y) in available, i[0], i[1])
            j=20
            while (not p) and j:
                r1=random.choice(tuple(connections))
                p=utils.bfs(r1[0], r1[1], lambda x, y: (x,y) in available, i[0], i[1])
                j-=1
            walkable.update(p)
    r1=random.choice(tuple(connections))
    for i in connections:
        if not utils.bfs(r1[0], r1[1], lambda x, y: (x,y) in walkable, i[0], i[1]):
            print("Cannot ensure connectivity. Map FUBAR.")
            break
    #add some extra paths
    pathpoints=tuple(walkable|available)
    newpaths=0
    for i in range(300):
        a=random.choice(pathpoints)
        b=random.choice(pathpoints)
        if len(utils.bfs(a[0], a[1], lambda x, y: (x,y) in walkable, b[0], b[1]))>=(abs(a[1]-b[1])+abs(a[0]-b[0]))*2:
            p=utils.bfs(b[0], b[1], (lambda x, y: (x,y) in pathpoints) if i<200 else (lambda x, y: (x,y) in available), a[0], a[1])
            if p:
                newpaths+=1
                walkable.update(p)
                if newpaths>=connectivity: break
    return (walkable,rooms)

def rooms1(width,height,ox=0,oy=0,sparsity=160,room_increments=8):
    """Some nice rooms with passages between them."""
    #randomly place around some rooms
    roompoints=set(zip(list(range(3+ox,width-3+ox))*(height-6),sorted(list(range(3+oy,height-3+oy))*(width-6))))
    available=set(zip(list(range(1+ox,width-1+ox))*(height-2),sorted(list(range(1+oy,height-1+oy))*(width-2))))
    walkable=set()
    rooms=set()
    centers=set()
    corners=set()
    for i in range(width*height//sparsity):
        if not roompoints: break
        r=random.choice(tuple(roompoints))
        centers.add(r)
        desired_growth=random.randint(0,6)+random.randint(0,6)
        x=range(-1,2)
        y=range(-1,2)
        valid_dirs=[0,1,2,3]
        for j in range(desired_growth):
            if not valid_dirs: break
            d=random.choice(valid_dirs)
            cr=zip(list(x) if d%2 else ((min(x)-1,max(x)+2)[d<2],)*len(y), ((min(y)-1,max(y)+2)[d<2],)*len(x) if d%2 else list(y))
            if all(map(lambda q: ts(q,r) in available, cr)):
                if d%2: y=range(min(y)-(d>1),max(y)+1+(d<2))
                else: x=range(min(x)-(d>1),max(x)+1+(d<2))
            else:valid_dirs.remove(d)
        for a in x:
            for b in y:
                rooms.add((r[0]+a,r[1]+b))
                walkable.add((r[0]+a,r[1]+b))
        for a in range(min(x)-1,max(x)+2):
            for b in range(min(y)-1,max(y)+2):
                available.discard((r[0]+a,r[1]+b))
        for a in range(min(x)-3,max(x)+4):
            for b in range(min(y)-3,max(y)+4):
                roompoints.discard((r[0]+a,r[1]+b))
        for a in (min(x)-1,min(x),max(x)+1,max(x)+2):
            for b in (min(y)-1,min(y),max(y)+1,max(y)+2):
                corners.add((r[0]+a,r[1]+b))
    #make some corridors between rooms to ensure connectivity
    not_corners = set(zip(list(range(1+ox,width-1+ox))*(height-2),sorted(list(range(1+oy,height-1+oy))*(width-2)))) - corners
    for a in centers:
        for b in centers:
            p=utils.bfs(a[0], a[1], lambda x, y: (x,y) in walkable, b[0], b[1])
            if (not p) or len(p)>=(abs(a[1]-b[1])+abs(a[0]-b[0]))*2:
                walkable.update(utils.bfs(b[0], b[1], lambda x,y: (x,y) in not_corners, a[0], a[1]))
    return (walkable,rooms)
    
def rooms2(width,height,ox=0,oy=0,sparsity=200,room_increments=9,connectivity=1):
    """Some nice rooms with passages between them."""
    #randomly place around some rooms, exactly the same as rooms1
    roompoints=set(zip(list(range(3+ox,width-3+ox))*(height-6),sorted(list(range(3+oy,height-3+oy))*(width-6))))
    available=set(zip(list(range(1+ox,width-1+ox))*(height-2),sorted(list(range(1+oy,height-1+oy))*(width-2))))
    extra_unavailable=set(zip(list(range(1+ox,width-1+ox))*(height-2),sorted(list(range(1+oy,height-1+oy))*(width-2))))
    walkable=set()
    rooms=set()
    centers=set()
    corners=set()
    connections=set()
    for i in range(width*height//sparsity):
        if not roompoints: break
        r=random.choice(tuple(roompoints))
        centers.add(r)
        desired_growth=random.randint(0,6)+random.randint(0,6)
        x=range(-1,2)
        y=range(-1,2)
        valid_dirs=[0,1,2,3]
        connection_draw=set()
        for j in range(desired_growth):
            if not valid_dirs: break
            d=random.choice(valid_dirs)
            cr=zip(list(x) if d%2 else ((min(x)-1,max(x)+2)[d<2],)*len(y), ((min(y)-1,max(y)+2)[d<2],)*len(x) if d%2 else list(y))
            if all(map(lambda q: ts(q,r) in extra_unavailable, cr)):
                if d%2: y=range(min(y)-(d>1),max(y)+1+(d<2))
                else: x=range(min(x)-(d>1),max(x)+1+(d<2))
            else:valid_dirs.remove(d)
        for a in x:
            for b in y:
                rooms.add((r[0]+a,r[1]+b))
                walkable.add((r[0]+a,r[1]+b))
        for a in range(min(x)-1,max(x)+2):
            for b in range(min(y)-1,max(y)+2):
                available.discard((r[0]+a,r[1]+b))
                connection_draw.add((r[0]+a,r[1]+b))
        for a in range(min(x)-3,max(x)+4):
            for b in range(min(y)-3,max(y)+4):
                extra_unavailable.discard((r[0]+a,r[1]+b))
        for a in range(min(x)-6,max(x)+7):
            for b in range(min(y)-6,max(y)+7):
                roompoints.discard((r[0]+a,r[1]+b))
        for a in (min(x)-1,min(x),max(x)+1,max(x)+2):
            for b in (min(y)-1,min(y),max(y)+1,max(y)+2):
                corners.add((r[0]+a,r[1]+b))
        #add a few exits
        connection_draw=tuple(connection_draw-rooms-corners)
        for i in random.sample(connection_draw,random.randint(2,4)):
            for d in utils.offsets4:
                if ts(i,d) in rooms: break
            if (i[0]-d[0],i[1]-d[1]) in available:
                walkable.add(i)
                walkable.add((i[0]-d[0],i[1]-d[1]))
                connections.add((i[0]-d[0],i[1]-d[1]))
    #make some paths between them
    ct=tuple(connections)
    for a in connections:
        for b in random.sample(ct,connectivity):
            walkable.update(utils.bfs(b[0], b[1], lambda x,y: (x,y) in available, a[0], a[1]))
    for a in connections:
        for b in random.sample(ct,connectivity*3):
            if len(utils.bfs(b[0], b[1], lambda x,y: (x,y) in walkable, a[0], a[1]))>(abs(a[0]-b[0])+abs(a[1]-b[1]))*3:
                walkable.update(utils.bfs(b[0], b[1], lambda x,y: (x,y) in available, a[0], a[1]))
    not_corners = set(zip(list(range(1+ox,width-1+ox))*(height-2),sorted(list(range(1+oy,height-1+oy))*(width-2)))) - corners
    for a in centers:
        r1=random.choice(ct)
        p=utils.bfs(a[0], a[1], lambda x, y: (x,y) in walkable, r1[0], r1[1])
        if (not p) or len(p)>=(abs(a[1]-b[1])+abs(a[0]-b[0]))*3:
            walkable.update(utils.bfs(r1[0], r1[1], lambda x,y: (x,y) in not_corners, a[0], a[1]))
    return (walkable,rooms)
    
def dragon_lair(width,height,ox=0,oy=0,size=9, jag=5, room_num=4):
    available=set(zip(list(range(1+ox,width-1+ox))*(height-2),sorted(list(range(1+oy,height-1+oy))*(width-2))))
    center=(width//2+random.randint(-2,2), height//2+random.randint(-2,2))
    walkable=set()
    connections=[set() for x in range(4)]
    rooms=set()
    lair=set()
    #make central lair
    for i in range(-size,1+size):
        for j in range(-size,1+size):
            if i*i+j*j<=size*size and (center[0]+i,center[1]+j) in available:
                walkable.add((center[0]+i,center[1]+j))
                lair.add((center[0]+i,center[1]+j))
    pc = tuple(lair)
    for n in range(int(len(lair)/jag/jag*2)):
        p = random.choice(pc)
        r = random.randint(jag-1,jag+1)
        for i in range(-r,1+r):
            for j in range(-r+abs(i),r-abs(i)+1):
                if (p[0]+i,p[1]+j) in available:
                    walkable.add((p[0]+i,p[1]+j))
                    lair.add((p[0]+i,p[1]+j))
                    if i==-r and p[0]<center[0]: connections[1].add((p[0]+i,p[1]+j))
                    if i==r and p[0]>center[0]: connections[2].add((p[0]+i,p[1]+j))
    #make side rooms
    rn_max=random.randint(-1,1)+room_num
    for r in range(0,rn_max):
        b=int(height/(rn_max*2)*(r*2+1))+random.randint(-1,1) #vertical coord of room
        for a in range(int(width//2.2)):
            if (a,b) in lair: break
            a_max=a
        if (a,b) not in lair:
            for i in range(b,height//2,1 if b<height//2 else -1): walkable.add((int(width//2.2),i))
        a=random.randint(int(a_max/4),int(a_max/4*3)) #horizontal coord of room
        for i in range(a,a_max+1): walkable.add((i,b))
        for j in range(random.randint(-2,-1)+b,b+random.randint(2,3)):
            for i in range(random.randint(-2,-1)+a,a+random.randint(2,3)):
                if (i,j) in available:
                    walkable.add((i,j))
                    rooms.add((i,j))
        connections[0].add((a,b))
    rn_max=random.randint(-1,1)+room_num
    for r in range(0,rn_max):
        b=int(height/(rn_max*2)*(r*2+1))+random.randint(-1,1) #vertical coord of room
        for a in range(width,width-int(width//2.2),-1):
            if (a,b) in lair: break
            a_max=a
        if (a,b) not in lair:
            for i in range(b,height//2,1 if b<height//2 else -1): walkable.add((width-int(width//2.2),i))
        a=random.randint(int(width-((width-a_max)/4*3)),int(width-((width-a_max)/4))) #horizontal coord of room
        for i in range(a_max, a+1): walkable.add((i,b))
        for j in range(random.randint(-2,-1)+b,b+random.randint(2,3)):
            for i in range(random.randint(-2,-1)+a,a+random.randint(2,3)):
                if (i,j) in available:
                    walkable.add((i,j))
                    rooms.add((i,j))
        connections[3].add((a,b))
    available-=lair
    #connect the side rooms
    rl=sorted(tuple(connections[0]), key=lambda x: x[1])
    for p in range(len(rl)-1,-1,-1):
        path=utils.bfs(rl[p][0], rl[p][1], lambda x,y: (x,y) in available, rl[p-1][0], rl[p-1][1])
        walkable.update(path)
        available=available-set(path[1:-1])
    rl=sorted(tuple(connections[3]), key=lambda x: x[1])
    for p in range(len(rl)-1,-1,-1):
        path=utils.bfs(rl[p][0], rl[p][1], lambda x,y: (x,y) in available, rl[p-1][0], rl[p-1][1])
        walkable.update(path)
        available=available-set(path[1:-1])
    return (walkable, rooms, lair, center)
                

def make_walls(walkable):
    objects={}
    for c in walkable:
        objects[c]=[entity.Floor(*c)]
        for d in utils.offsets8:
            if ts(c,d) not in walkable: objects[ts(c,d)]=[entity.Wall(*ts(c,d))]
    return objects
            
            
def ts(a,b):
    return (a[0]+b[0],a[1]+b[1])
    
methods = (rooms2,rooms1,maze1)
mazes = (maze1,) #close quarters levels (spawn a reduced amount of crossbow guards)
lair_methods = (dragon_lair,)